const t = 5; 

class BTree {
    constructor (t){
       this.t = t;
       this.rootNode = new Node(this.t);
   }

    insertKeyInTree(key){
        let node = this.findNodeForInsertingKey(key, this.rootNode);
        this.insertKey(key, node);
        if (node.isFullNode())
            this.rebalanceTree(node);
    }

    rebalanceTree(node){
        if (node.isRootNode())
            this.increaseDepthOfTree();
        else
            this.splitInnerNodeToSubnodes(node);
        if (node.parent !== null){
            let parent = node.parent;
            if(parent.isFullNode())
                return this.rebalanceTree(parent);
        }
    }
    
    increaseDepthOfTree(){
        let leftNode = new Node(this.t);
        let rightNode = new Node(this.t);
        let middleOfNode = Math.floor(this.rootNode.count/2);
        let middleOfChilds = Math.floor(this.rootNode.countChilds/2);
        let leftKeys = this.rootNode.keys.slice(0, middleOfNode);
        let rightKeys = this.rootNode.keys.slice(middleOfNode+1, this.rootNode.keys.length);
        this.insertArrayOfKeysInNode(leftKeys, leftNode);
        this.insertPointersOnChildsAndParent(0, middleOfChilds, leftNode);
        this.insertArrayOfKeysInNode(rightKeys, rightNode)
        this.insertPointersOnChildsAndParent(middleOfChilds, this.rootNode.countChilds, rightNode)
        this.updateKeysInRootNode(middleOfNode, this.rootNode);
        this.insertPointerOnChild(this.rootNode, leftNode);
        this.insertPointerOnChild(this.rootNode, rightNode);
        leftNode.parent = this.rootNode;
        rightNode.parent = this.rootNode;
    }

    insertPointersOnChildsAndParent(start, end, node){
        for (let i = start; i < end; i++){
            let child = this.rootNode.childs[i];
            this.insertPointerOnChild(node, child);
            child.parent = node;
        }
    }

    insertPointerOnChild(node, child){ //OK
        node.childs[node.countChilds] = child;
        node.countChilds++;
        node.childs.sort(function(a, b) {
            return a.keys[0] - b.keys[0];
        });   
        return node;
    }

    updateKeysInRootNode(middle, node){ //OK
        node.keys[0] = node.keys[middle];
        for (let i = 1; i<node.keys.length; i++){
        delete node.keys[i];
        }
        node.count = 1;
        let length = this.rootNode.countChilds;
        for(let i = 0; i < length; i++){
            delete this.rootNode.childs[i];
            this.rootNode.countChilds--;
        }
    }

    insertArrayOfKeysInNode(keys, node){ //OK
        for (let i=0; i<keys.length; i++){
            this.insertKey(keys[i], node)
        }
    } 

    splitInnerNodeToSubnodes(node){
        let rightNode = new Node(this.t);
        let middleOfNode = Math.floor(node.count/2);
        let middleOfChilds = Math.floor(node.countChilds/2);
        let newNodeKeys = node.keys.slice(0, middleOfNode);
        let keyToParent = node.keys[middleOfNode];
        let rightKeys = node.keys.slice(middleOfNode+1, node.keys.length);
        let parent = node.parent;
        this.insertArrayOfKeysInNode(rightKeys, rightNode);
        for (let i = middleOfChilds; i < node.countChilds; i++){
            let child = node.childs[i];
            this.insertPointerOnChild(rightNode, child);
            child.parent = rightNode;
            delete node.childs[i];
        }
        this.insertKey(keyToParent, parent);
        this.insertPointerOnChild(parent, rightNode);
        rightNode.parent = parent;
        for (let i = newNodeKeys.length; i < node.keys.length; i++){
            delete node.keys[i];
            node.count--
        }
    }

    findNodeForInsertingKey(key, node){ 
        let index = 0
        if (node.countChilds > 0){
        for (let i = 0; i < node.count; i++){
            if (key > node.keys[i])
                index++
            else
                break
        }
        let child = node.childs[index];
            return this.findNodeForInsertingKey(key, child)
        } else
            return node;
    }

    insertKey(key, node){ //OK
        let position = node.count;
        while (position > 0 && node.keys[position-1] > key){
            node.keys[position] = node.keys[position-1];
            position--;
        } 
        node.keys[position] = key;
        node.count++;
        return node;
    }
}

class Node {
    constructor(t){
        this.t = t;
        this.keys = new Array(this.t);
        this.childs = new Array(this.t+1)
        this.parent = null; 
        this.count = 0;
        this.countChilds = 0;
    }

    isRootNode(){
        if(this.parent === null)
            return true;
        else
            return false;
    }

    isFullNode(){
        if(this.count === t){
            return true;
        }
        else
            return false;
    }

    isLeaf(){
        if((this.parent !== null) && (this.countChilds == 0))
        return true;
            else
        return false;  
    }
}

let tree = new BTree(t);

tree.insertKeyInTree(56);
tree.insertKeyInTree(98);
tree.insertKeyInTree(34);
tree.insertKeyInTree(41);
tree.insertKeyInTree(34);
tree.insertKeyInTree(39);
tree.insertKeyInTree(16);
tree.insertKeyInTree(20);
tree.insertKeyInTree(5);
tree.insertKeyInTree(1);
tree.insertKeyInTree(3);
tree.insertKeyInTree(8);
tree.insertKeyInTree(25);
tree.insertKeyInTree(19);
tree.insertKeyInTree(21); 
tree.insertKeyInTree(51); 
tree.insertKeyInTree(59); 
tree.insertKeyInTree(78); 
tree.insertKeyInTree(100); 
tree.insertKeyInTree(111);
tree.insertKeyInTree(123);
tree.insertKeyInTree(92);
tree.insertKeyInTree(97);
tree.insertKeyInTree(37);
tree.insertKeyInTree(62);
tree.insertKeyInTree(87);
tree.insertKeyInTree(43);
tree.insertKeyInTree(29);
tree.insertKeyInTree(31);
tree.insertKeyInTree(48);
tree.insertKeyInTree(24);
tree.insertKeyInTree(23);
tree.insertKeyInTree(19);
tree.insertKeyInTree(83);
tree.insertKeyInTree(68);
tree.insertKeyInTree(59);
tree.insertKeyInTree(37);
tree.insertKeyInTree(23);
tree.insertKeyInTree(590);
tree.insertKeyInTree(232);
tree.insertKeyInTree(387);
tree.insertKeyInTree(390);
tree.insertKeyInTree(493);
tree.insertKeyInTree(345);
tree.insertKeyInTree(932);
tree.insertKeyInTree(322);
tree.insertKeyInTree(320);
tree.insertKeyInTree(902);
tree.insertKeyInTree(245);
tree.insertKeyInTree(298);
tree.insertKeyInTree(443);
tree.insertKeyInTree(986);

console.log(tree);